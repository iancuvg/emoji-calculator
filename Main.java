import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.util.*;
import java.util.stream.Collectors;

public class Main {

    // dictionary

    private static final String HUNDRED = "💯";
    private static final String ZERO = "0️⃣";
    private static final String ONE = "1️⃣";
    private static final String TWO = "2️⃣";
    private static final String THREE = "3️⃣";
    private static final String FOUR = "4️⃣";
    private static final String FIVE = "5️⃣";
    private static final String SIX = "6️⃣";
    private static final String SEVEN = "7️⃣⃣";
    private static final String EIGHT = "8️⃣⃣";
    private static final String NINE = "9️⃣⃣";
    private static final String TEN = "\uD83D\uDD1F";
    private static final String EIGHT_BALL = "\uD83C\uDFB1";
    private static final String ADDITION_WORD = "plus";
    private static final String ADDITION_EMOJI = "➕";
    private static final String SUBTRACTION_WORD = "minus";
    private static final String SUBTRACTION_EMOJI = "➖";
    private static final String MULTIPLICATION_EMOJI1 = "✖️";       // looks identical, but an additional space makes them different
    private static final String MULTIPLICATION_EMOJI2 = "✖";        // looks identical
    private static final String MULTIPLICATION_LETTER = "x";
    private static final String MULTIPLICATION_WORD = "times";
    private static final String DIVISION_EMOJI = "➗";
    private static final String DIVISION_WORD = "divided by";
    private static final String DONT_KNOW_EMOJI = "\uD83E\uDD37";


    private static String encoding(Object result) {
        List<Integer> stocks = result.toString().
                chars().
                mapToObj(i -> (Integer)i - 48).
                collect(Collectors.toList());

        for(int i = 0 ; i < stocks.size() ; i++) {
            if((i + 1) < stocks.size() && stocks.get(i) == 1 && stocks.get(i + 1) == 0) {
                stocks.set(i, 10);
                stocks.set(i + 1, -1);
                if((i + 2) < stocks.size() && stocks.get(i + 2) == 0) {
                    stocks.set(i, 100);
                    stocks.set(i + 2, -1);
                }
            }
        }
        List<Integer> stocksFinal = new ArrayList<>();
        for (Integer stock : stocks) {
            if (!stock.equals(-1)) {
                stocksFinal.add(stock);
            }
        }
        List<String> stocksFinalStrings = new ArrayList<>();
        for(Integer stock : stocksFinal) {
            stocksFinalStrings.add(String.valueOf(stock));
        }
        Random random = new Random();
        for(String stock : stocksFinalStrings) {
            if(stock.equals("0"))
                stocksFinalStrings.set(stocksFinalStrings.indexOf(stock),ZERO);
            if(stock.equals("1"))
                stocksFinalStrings.set(stocksFinalStrings.indexOf(stock),ONE);
            if(stock.equals("2"))
                stocksFinalStrings.set(stocksFinalStrings.indexOf(stock),TWO);
            if(stock.equals("3"))
                stocksFinalStrings.set(stocksFinalStrings.indexOf(stock),THREE);
            if(stock.equals("4"))
                stocksFinalStrings.set(stocksFinalStrings.indexOf(stock),FOUR);
            if(stock.equals("5"))
                stocksFinalStrings.set(stocksFinalStrings.indexOf(stock),FIVE);
            if(stock.equals("6"))
                stocksFinalStrings.set(stocksFinalStrings.indexOf(stock),SIX);
            if(stock.equals("7"))
                stocksFinalStrings.set(stocksFinalStrings.indexOf(stock),SEVEN);
            if(stock.equals("8"))
                stocksFinalStrings.set(stocksFinalStrings.indexOf(stock),random.nextBoolean() ? EIGHT : EIGHT_BALL);
            if(stock.equals("9"))
                stocksFinalStrings.set(stocksFinalStrings.indexOf(stock),NINE);
            if(stock.equals("10"))
                stocksFinalStrings.set(stocksFinalStrings.indexOf(stock),TEN);
            if(stock.equals("100"))
                stocksFinalStrings.set(stocksFinalStrings.indexOf(stock),HUNDRED);
        }
        if(stocksFinalStrings.get(0).equals("-3"))
            stocksFinalStrings.set(0,"-");
        return String.join("",stocksFinalStrings);
    }

    private static String decoding(String input) {
        String inputs = input;

        if(input.contains(ADDITION_WORD)) {
            inputs = input.replaceAll(ADDITION_WORD,"+");
        }
        if(input.contains(ADDITION_EMOJI)) {
            inputs = inputs.replaceAll(ADDITION_EMOJI,"+");
        }
        if(input.contains(SUBTRACTION_WORD)) {
            inputs = inputs.replaceAll(SUBTRACTION_WORD,"-");
        }
        if(input.contains(SUBTRACTION_EMOJI)) {
            inputs = inputs.replaceAll(SUBTRACTION_EMOJI,"-");
        }
        if(input.contains(MULTIPLICATION_EMOJI1)) {
            inputs = inputs.replaceAll(MULTIPLICATION_EMOJI1,"*");
        }
        if(input.contains(MULTIPLICATION_EMOJI2)) {
            inputs = inputs.replaceAll(MULTIPLICATION_EMOJI2,"*");
        }
        if(input.contains(MULTIPLICATION_LETTER)) {
            inputs = inputs.replaceAll(MULTIPLICATION_LETTER,"*");
        }
        if(input.contains(MULTIPLICATION_WORD)) {
            inputs = inputs.replaceAll(MULTIPLICATION_WORD,"*");
        }
        if(input.contains(DIVISION_EMOJI)) {
            inputs = inputs.replaceAll(DIVISION_EMOJI,"/");
        }
        if(input.contains(DIVISION_WORD)) {
            inputs = inputs.replaceAll(DIVISION_WORD,"/");
        }
        if(input.contains(ZERO)) {
            inputs = inputs.replaceAll(ZERO,"0");
        }
        if(input.contains(ONE)) {
            inputs = inputs.replaceAll(ONE,"1");
        }
        if(input.contains(TWO)) {
            inputs = inputs.replaceAll(TWO,"2");
        }
        if(input.contains(THREE)) {
            inputs = inputs.replaceAll(THREE,"3");
        }
        if(input.contains(FOUR)) {
            inputs = inputs.replaceAll(FOUR,"4");
        }
        if(input.contains(FIVE)) {
            inputs = inputs.replaceAll(FIVE,"5");
        }
        if(input.contains(SIX)) {
            inputs = inputs.replaceAll(SIX,"6");
        }
        if(input.contains(SEVEN)) {
            inputs = inputs.replaceAll(SEVEN,"7");
        }
        if(input.contains(EIGHT)) {
            inputs = inputs.replaceAll(EIGHT,"8");
        }
        if(input.contains(EIGHT_BALL)) {
            inputs = inputs.replaceAll(EIGHT_BALL,"8");
        }
        if(input.contains(NINE)) {
            inputs = inputs.replaceAll(NINE,"9");
        }
        if(input.contains(TEN)) {
            inputs = inputs.replaceAll(TEN,"10");
        }
        if(input.contains(HUNDRED)) {
            inputs = inputs.replaceAll(HUNDRED,"100");
        }

        return inputs;
    }

    public static void main(String[] args) throws ScriptException {
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        String inputs = decoding(input);

        try {
            ScriptEngineManager scriptEngineManager = new ScriptEngineManager();
            ScriptEngine scriptEngine = scriptEngineManager.getEngineByName("js");
            Object result = scriptEngine.eval(inputs);
            if (result.toString().equals("NaN") || result.toString().equals("Infinity"))
                throw new ArithmeticException();
            else {
                if(result.toString().contains(".")){
                    String resultEncrypted = encoding(((Double) result).intValue());      // in case 'result' variable is a real number.
                    System.out.println(resultEncrypted);
                } else {
                    String resultEncrypted = encoding(result);
                    System.out.println(resultEncrypted);
                }
            }
        } catch(ArithmeticException e) {
            System.out.println(DONT_KNOW_EMOJI);
        }

    }
}

