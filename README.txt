================

EMOJI CALCULATOR
================

First of all, I had to think on how do I find out how these emojis are coded. So, I sent to myself a message with all the emojis that will help in order to make an Emoji 'Dictionary' for solving the puzzle.
As you can see from the beginning in the code, I declared these dictionary variables as constants. I don't need to change them, they're just final. After that, I observed that the puzzle wants me to read a line of a mathematic calculation. Ok, so that's the input, a String variable which is going to be read in the console, until it gets to the second line. Input can come up with a ton of numbers, ready for giving an output expression.
The method decoding is for setting all these 'special characters' found in the Emoji Dictionary to normal characters, from the 'input' variable. Let's have an example :

Before: input = "4️⃣2️⃣➕ 🎱 ➕ 2️⃣5️⃣ ✖️ 2️⃣ ➕ 1️⃣0️⃣0️⃣0️⃣1️⃣ ➖ 1️⃣"
After: input = "42 +8 + 25 * 2 + 10001 - 1"

It doesn't matter whether you put spaces between numbers and mathematical symbols, this method is just for decoding.
I had no idea how to make a mathematic expression from a String. Luckily, I remembered a method from Python language called eval(), and I googled a Java version for this method. I found that there is a class called ScriptEngine and it evaluates as a Javascript String. Apparently, this methods finds out to be simmilar in those programming languages. Starting from the previous example, we go further and we will have :

Before 'evaluating': "42 + 8 + 25 * 2 + 10001 - 1"
After 'evaluating': 10100 -> It's an Object variable in order to choose what fits the best for the 'evaluated' output. If it was "12/7", the result would be a Double variable. Anyway, in this case, the result is an Integer.

Perfect, now I have to encode the result on how this puzzle wants.
This part comes a bit juicy when I need to have a greedy approach. What I'm trying to say is that the puzzle mentioned that the result should have as few as possible emojis. For instance :

result: 10100 = 1️⃣0️⃣1️⃣0️⃣0️⃣
also result: 10100 = 🔟 💯

I have to choose the second result because of the fewer emojis it has. The method encode is made for that.
So, I've made a list of all digits appeared in result. I need to traverse all those digits in order to find out if any combinations of 10 or 100 exists in the result. If exists, the position where the digit 1 was is going to be assigned with that combination, and the other position/s will be assigned with -1. Unfortunately, I can't remove directly the positions. This is suspicious as list becomes shorter after that and the element next to removed will not be processed. After setting up the list, I'm going to create another list which will stack all the elements which are not equal to -1. For example :

result: 10100 -> list: 1, 0, 1, 0, 0 --processing--> list: 10, -1, 100, -1, -1 --finally--> list: 10, 100

Now it comes the encoding part, which is a simple association between the list and every 'Dictionary' word. Note that eight may come as the simple Emoji EIGHT or the EIGHT_BALL. So I said 'Ok, it's time for the compiler to choose between them!'
Then, I had to check if the result can be processed. If not the program will simply throw a NullPointerException, and print out the DONT_KNOW_EMOJI, as the puzzle says. The puzzle didn't mention anything about double results. So, if the result is going to be a double value, it will convert it to an int value.


as a command line: 	-> for compiling: javac Main.java
			-> for running: java.Main.java

I also shared the IntelliJ project.

With all that said, thanks for reading this stuff.
I hope it will make you understand my own way to solve this puzzle.

